//
//  ViewController.swift
//  Asynchronous-Programming-Model
//
//  Created by Patricio GUZMAN on 10/5/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

struct Item {
    let uiImage: UIImage!
}

class ViewController: UIViewController {
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    var Items = [Item?](repeating: nil, count: 4)
    var imagesLoaded: Int = 0
    var imageToPass: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUIImage(index: 0, url: "https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg")
        getUIImage(index: 1, url: "https://www.nasa.gov/images/content/650137main_pia15416b-43_full.jpg")
        getUIImage(index: 2, url: "https://assets.vg247.com/current//2017/03/mass_effect_andromeda_4k_screnshot_06.png")
        getUIImage(index: 3, url: "https://www.nasa.gov/sites/default/files/thumbnails/image/hs-2015-02-a-hires_jpg.jpg")
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myBigImageIdentifier" {
            if let myScrollViewController: SecondViewController = segue.destination as? SecondViewController {
                myScrollViewController.setPhotoToDisplay(photo: imageToPass!)
            }
        }
    }
    
    func getUIImage(index: Int, url: String) {
        if (UIApplication.shared.isNetworkActivityIndicatorVisible == false) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        DispatchQueue.global(qos: .userInteractive).async {
            let start = Date()
            self.appendItem(index: index, url: url)
            let end = Date()
            print(end.timeIntervalSince(start))
            self.imagesLoaded += 1
            if (self.imagesLoaded == 4) {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            DispatchQueue.main.async {
                self.myCollectionView.reloadData()
            }
        }
    }

    func appendItem(index: Int, url: String) {
        do {
            Items[index] = try Item(uiImage: UIImage(data: Data(contentsOf: URL(string: url)!)))
        }
        catch {
            let alert = UIAlertController(title: "Error", message: "Something went wrong loading from this url : \(url)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }

    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "myCollectionCellIdentifier"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! MyCollectionViewCell
        

        if let item = self.Items[indexPath.row] {
            cell.setImage(item: item)
        }
        else {
            cell.renderActivityMonitor()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected")
        imageToPass = Items[indexPath.row]?.uiImage
        self.performSegue(withIdentifier: "myBigImageIdentifier", sender: nil)
    }
    
    
}
