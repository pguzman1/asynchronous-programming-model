//
//  MyCollectionViewCell.swift
//  Asynchronous-Programming-Model
//
//  Created by Patricio GUZMAN on 10/5/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

protocol MyCollectionViewCellDelegate {
    
}

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

    
    func setImage(item: Item) {
        imageView.image = item.uiImage
        activityIndicator.stopAnimating()
    }

    func renderActivityMonitor() {
        activityIndicator.center = imageView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        imageView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.activityIndicator.stopAnimating()
        }
    }
    
}
