//
//  SecondViewController.swift
//  Asynchronous-Programming-Model
//
//  Created by Patricio GUZMAN on 10/5/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var myUiview: UIImageView?
    var photoToDisplay: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        myUiview = UIImageView(image: photoToDisplay)
        myUiview?.frame = view.bounds
        myUiview?.contentMode = .scaleAspectFit
        scrollView.contentSize = (myUiview?.frame.size)!
        scrollView.maximumZoomScale = 1
        scrollView.minimumZoomScale = 0.1
        scrollView.addSubview(myUiview!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        myUiview = nil
    }
    
    func setPhotoToDisplay(photo: UIImage) {
        self.photoToDisplay = photo
    }
    
     func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return myUiview
    }
    
    override func viewDidLayoutSubviews() {
        myUiview?.frame = view.bounds
        myUiview?.contentMode = .scaleAspectFit
        scrollView.contentSize = (myUiview?.frame.size)!
        scrollView.maximumZoomScale = 1
        scrollView.minimumZoomScale = 0.1
    }
    
    
}
